const { promisify } = require('util');
const { readFile, readdir, writeFile } = require('fs');
const convert = require('heic-convert');

const readDirAsync = promisify(readdir);
const readFileAsync = promisify(readFile);
const writeFileAsync = promisify(writeFile);

const heicImagesSrc = './src';
const heicImagesDist = './dist';
const desiredFormat = 'JPEG';

const uint8ArrayUtf8ByteString = (array, start, end) => {
    return String.fromCharCode(...array.slice(start, end));
};
  
const isHeic = (file) => {
    const { buffer } = file;
    const brandMajor = uint8ArrayUtf8ByteString(buffer, 8, 12).replace('\0', ' ').trim();
  
    switch (brandMajor.toLowerCase()) {
      case 'mif1':
      case 'msf1':
      case 'heic':
      case 'heix':
      case 'hevc':
      case 'hevx':
        return true; // {ext: 'heic', mime: 'image/heic-sequence'};
    }
  
    return false;
};

readDirAsync(heicImagesSrc).then(async (heicImages) => {
    try {
        console.time('readFileAsync');
        const inputFiles = await Promise.all(heicImages.map(async (image) => ({
            filename: image,
            buffer: await readFileAsync(`${heicImagesSrc}/${image}`)
        })));
        console.timeEnd('readFileAsync');
        
        const filteredInput = inputFiles.filter(isHeic);
        console.log(`Filtered ${heicImages.length - filteredInput.length} HEIC files`);

        console.time('convert');
        const outputFiles = await Promise.all(filteredInput.map(async (file) => {
            const { filename, buffer } = file;
            console.log(`Converting image ${filename}`);

            try {
                return {
                    filename,
                    buffer: await convert({
                        buffer,
                        format: desiredFormat,
                        quality: 1
                    })
                };
            } catch (error) {
                console.error(`An error occured while trying to convert image ${filename}`);
            }
        }));
        console.log(`Finished converting ${filteredInput.length} HEIC buffers to ${desiredFormat} buffers`);
        console.timeEnd('convert');

        console.time('writeFileAsync');
        await Promise.all(outputFiles.map((file) => writeFileAsync(`${heicImagesDist}/${file.filename}.${desiredFormat}`, file.buffer)));
        console.timeEnd('writeFileAsync');
    } catch (error) {
        console.error(error);
    }
});